
from bs4 import BeautifulSoup
import re as re
import time
import json
import datetime
import webapp2
import logging


class LinkledinScrapper(webapp2.RequestHandler):
    def get_url(self, url_section):
        url_li_tags = url_section.find('div') if url_section else None
        return url_li_tags.find('a').get_text().strip() if url_li_tags else None

    def get_mobile(self, mobile_section):
        mobile_li_tags = mobile_section.find('ul') if mobile_section else None

        if mobile_li_tags:
            return mobile_li_tags.find_all('span')[0].get_text().strip() if mobile_li_tags.find_all('span') else None
        else:
            return None

    def get_email(self, email_section):
        email_li_tags = email_section.find('div') if email_section else None
        return email_li_tags.find('a').get_text().strip() if email_li_tags else None

    def get_name_location_profile(self, name_div):
        name_loc = name_div.find_all('div') if name_div else []
        name = None
        profile = None
        loc = None
        for val in name_loc:
            if val.find('h1', {'class': 'text-heading-xlarge inline t-24 v-align-middle break-words'}):
                name = val.find('h1', {'class': 'text-heading-xlarge inline t-24 v-align-middle break-words'}).get_text().strip()
                continue
            if val.find('span', {'class': 'text-body-small inline t-black--light break-words'}):
                loc = val.find('span', {'class': 'text-body-small inline t-black--light break-words'}).get_text().strip()
                continue
        if len(name_loc) > 1 and name_loc[1]:  # , {'class': 'text-body-medium break-words'}):
            profile = name_loc[1].get_text().strip()
        return name, profile, loc

    def get_experience(self, exp_section):
        experience = []
        if exp_section:
            exp_section = exp_section.find('ul')
            tags = exp_section.find_all('li')
            for li_tags in tags:
                job_title = None
                company_name = None
                joining_date = None
                section_exp = li_tags.find('section') if li_tags.find('section') else None
                if section_exp:
                    ul_tag_exp = section_exp.find('ul') if section_exp.find('ul') else None
                    if ul_tag_exp is None:
                        li_tags = section_exp.find('div')
                        if li_tags:
                            a_tags = li_tags.find('a')
                            if a_tags:
                                if a_tags.find('h3'):
                                    job_title = a_tags.find('h3').get_text().strip()
                                else:
                                    job_title = None
                                company_name = a_tags.find_all('p')[1].get_text().strip() if len(
                                    a_tags.find_all('p')) > 1 else None
                                if a_tags.find_all('h4'):
                                    joining_date = a_tags.find_all('h4')[0].find_all('span')[1].get_text().strip() if len(
                                        a_tags.find_all('h4')[0].find_all('span')) > 1 else None
                    else:
                        li_tags = section_exp.find('div')
                        if li_tags:
                            a_tags = li_tags.find('a')
                            if a_tags:
                                if a_tags.find('h3'):
                                    if a_tags.find('h3').find_all('span'):
                                        company_name = a_tags.find('h3').find_all('span')[1].get_text().strip() if len(
                                            a_tags.find('h3').find_all('span')) > 1 else None
                                else:
                                    company_name = None
                        li_tag_exp = ul_tag_exp.find_all('li')[0] if ul_tag_exp.find_all('li') else None
                        if li_tag_exp:
                            if li_tag_exp.find('h3'):
                                job_title = li_tag_exp.find('h3').find_all('span')[1].get_text().strip() if len(li_tag_exp.find('h3').find_all('span')) > 1 else None
                            else:
                                job_title = None
                            if li_tag_exp.find('h4', {'class': 'pv-entity__date-range t-14 t-black--light t-normal'}):
                                logging.info(li_tag_exp.find('h4', {'class': 'pv-entity__date-range t-14 t-black--light t-normal'}))
                                joining_date = li_tag_exp.find('h4', {'class': 'pv-entity__date-range t-14 t-black--light t-normal'}).find_all('span')[1].get_text().strip() if len(
                                    li_tag_exp.find('h4', {'class': 'pv-entity__date-range t-14 t-black--light t-normal'}).find_all('span')) > 1 else None
                # if exp_section:
                #     li_tags = li_tags.find('div')
                #
                #     if li_tags:
                #         a_tags = li_tags.find('a')
                #         if a_tags:
                #             if a_tags.find('h3'):
                #                 if a_tags.find('h3').find_all('span'):
                #                     company_name = a_tags.find('h3').find_all('span')[1].get_text().strip() if len(a_tags.find('h3').find_all('span')) > 1 else None
                #                 else:
                #                     job_title = a_tags.find('h3').get_text().strip()
                #             else:
                #                 job_title = None
                #
                #             company_name = a_tags.find_all('p')[1].get_text().strip() if len(
                #                 a_tags.find_all('p')) > 1 else None
                #             if a_tags.find_all('h4'):
                #                 joining_date = a_tags.find_all('h4')[0].find_all('span')[1].get_text().strip() if len(
                #                     a_tags.find_all('h4')[0].find_all('span')) > 1 else None
                exp = {"job_title": job_title, "company_name": company_name, "joining_date": joining_date}
                experience.append(exp)
        return experience

    def get_certification(self, certi_section):
        certification = []
        ul_tags = certi_section.find('ul') if certi_section else None
        li_tags = ul_tags.find_all('li') if ul_tags else []
        for li_tag in li_tags:
            company = None
            validity = None
            certi = None
            if li_tag.find('a'):
                certi = li_tag.find('a').find('h3').get_text().strip() if li_tag.find('a').find('h3') else None
                if li_tag.find('a').find_all('p'):
                    if li_tag.find('a').find_all('p')[0]:
                        if li_tag.find('a').find_all('p')[0].find_all('span')[1]:
                            company = li_tag.find('a').find_all('p')[0].find_all('span')[1].get_text().strip()
                        if len(li_tag.find('a').find_all('p')) > 1 and li_tag.find('a').find_all('p')[1]:
                            if len(li_tag.find('a').find_all('p')[1].find_all('span')) > 1 and li_tag.find('a').find_all('p')[1].find_all('span')[1]:
                                validity = li_tag.find('a').find_all('p')[1].find_all('span')[1].get_text().strip()
                else:
                    certi = li_tag.find('h3').get_text().strip() if li_tag.find('h3') else None
                    if li_tag.find_all('p'):
                        if li_tag.find_all('p')[0]:
                            if li_tag.find_all('p')[0].find_all('span')[1]:
                                company = li_tag.find_all('p')[0].find_all('span')[1].get_text().strip()
                            logging.info(len(li_tag.find_all('p')))
                            if len(li_tag.find_all('p')) > 1 and li_tag.find_all('p')[1]:
                                if len(li_tag.find_all('p')[1].find_all('span')) > 1 and \
                                        li_tag.find_all('p')[1].find_all('span')[1]:
                                    validity = li_tag.find_all('p')[1].find_all('span')[1].get_text().strip()
            else:
                certi = li_tag.find('h3').get_text().strip() if li_tag.find('h3') else None
                logging.info(certi)
                if li_tag.find_all('p'):
                    if li_tag.find_all('p')[0]:
                        if li_tag.find_all('p')[0].find_all('span')[1]:
                            company = li_tag.find_all('p')[0].find_all('span')[1].get_text().strip()
                        logging.info(len(li_tag.find_all('p')))
                        if len(li_tag.find_all('p')) > 1 and li_tag.find_all('p')[1]:
                            if len(li_tag.find_all('p')[1].find_all('span')) > 1 and li_tag.find_all('p')[1].find_all('span')[1]:
                                validity = li_tag.find_all('p')[1].find_all('span')[1].get_text().strip()

            certification.append(
                {
                    "certi": certi,
                    "company": company,
                    "validity": validity
                }
            )
        return certification

    def get_education_details(self, edu_section):
        education = []
        if edu_section:
            edu_section = edu_section.find('ul')
            tags = edu_section.find_all('li')
            for li_tags in tags:
                college_name = None
                degree = None
                if edu_section:
                    li_tags = li_tags.find('div')

                    if li_tags:
                        a_tags = li_tags.find('a')
                        if a_tags:
                            college_name = a_tags.find('h3').get_text().strip() if a_tags.find('h3') else None
                            if len(a_tags.find_all('p')) > 1:
                                if a_tags.find_all('p')[1].find_all('span')[0].get_text().strip() == 'Field Of Study':
                                    degree = a_tags.find_all('p')[1].find_all('span')[1].get_text().strip() if len(
                                        a_tags.find_all('p')[1].find_all('span')) > 1 else None
                            else:
                                degree = None
                edu = {"college_name": college_name, "degree": degree}
                education.append(edu)
        return education

    def get_skills(self, skill_section, soup):
        ol_tag = skill_section.find('ol') if skill_section else None
        skill_li_tags = ol_tag.find_all('li', {
            'class': 'pv-skill-category-entity__top-skill pv-skill-category-entity pb3 pt4 pv-skill-endorsedSkill-entity relative ember-view'}) if ol_tag else []
        skills = []

        for skill_li_tag in skill_li_tags:
            div_tag = skill_li_tag.find('div')
            p_tag = div_tag.find('p')
            skill = p_tag.find('span').get_text().strip()
            skills.append(skill)

        tech_skills = soup.find_all('div',
                                    {'class': 'pv-skill-category-list pv-profile-section__section-info mb6 ember-view'})
        for tech_skill in tech_skills:
            tech_ol_tag = tech_skill.find('ol') if tech_skill else None
            tech_skill_li_tags = tech_ol_tag.find_all('li', {
                'class': 'pv-skill-category-entity pv-skill-category-entity--secondary pt4 pv-skill-endorsedSkill-entity relative ember-view'}) if tech_ol_tag else []

            for tech_skill_li_tag in tech_skill_li_tags:
                div_tag = tech_skill_li_tag.find('div')
                p_tag = div_tag.find('p')
                skill = p_tag.find('span').get_text().strip()
                skills.append(skill)
        return skills

    def get_profile_pic(self, profile_pic_div):
        return profile_pic_div.find('img')['src'] if profile_pic_div else None

    def get_volunteer_details(self, volunteer_section):
        volunteer = []
        ul_tag = volunteer_section.find('ul') if volunteer_section else None
        li_tags = ul_tag.find_all('li') if ul_tag else []
        for li_tag in li_tags:
            a_tag = li_tag.find('a')
            volunteer_title = a_tag.find('h3').get_text().strip() if a_tag else None
            if a_tag.find('h4').find_all('span') and len(a_tag.find('h4').find_all('span')) > 1:
                company_name = a_tag.find('h4').find_all('span')[1].get_text().strip()
            else:
                company_name = None
            volunteer_data = {
                "volunteer_title": volunteer_title,
                "company_name": company_name
            }
            volunteer.append(volunteer_data)
        return volunteer

    def post(self):
        data = json.loads(self.request.body)
        src = data['htmldata']
        soup = BeautifulSoup(src, 'html.parser')

        url_section = soup.find('section', {'class': 'pv-contact-info__contact-type ci-vanity-url'})
        url = self.get_url(url_section)

        mobile_section = soup.find('section', {'class': 'pv-contact-info__contact-type ci-phone'})
        mobile = self.get_mobile(mobile_section)

        email_section = soup.find('section', {'class': 'pv-contact-info__contact-type ci-email'})
        email = self.get_email(email_section)

        name_div = soup.find('div', {'class': 'pv-text-details__left-panel mr5'})
        name, profile, loc = self.get_name_location_profile(name_div)

        profile_pic_div = soup.find('div', {'class': 'pv-top-card--photo text-align-left'})
        profile_pic = self.get_profile_pic(profile_pic_div)

        logging.info(name)
        logging.info(profile)
        logging.info(loc)

        exp_section = soup.find('section', {'id': 'experience-section'})
        experience = self.get_experience(exp_section)
        logging.info(experience)


        certi_section = soup.find('section', {'id': 'certifications-section'})
        certification = self.get_certification(certi_section)
        logging.info(certification)

        edu_section = soup.find('section', {'id': 'education-section'})
        education = self.get_education_details(edu_section)
        logging.info(education)
        if soup.find('section', {'class': 'pv-profile-section pv-skill-categories-section artdeco-card mt4 p5 first-degree ember-view'}):
            skill_section = soup.find('section', {'class': 'pv-profile-section pv-skill-categories-section artdeco-card mt4 p5 first-degree ember-view'})
        else:
            skill_section = soup.find('section', {
                'class': 'pv-profile-section pv-skill-categories-section artdeco-card mt4 p5 ember-view'})
        skills = self.get_skills(skill_section, soup)
        logging.info(skills)

        volunteer_section = soup.find('section', {'class': 'pv-profile-section volunteering-section ember-view'})
        volunteer_data = self.get_volunteer_details(volunteer_section)

        response = {
            "name": name,
            "profile": profile,
            "loc": loc,
            "experience": experience,
            "certification": certification,
            "url": url,
            "mobile": mobile,
            "email": email,
            "education": education,
            "skills": skills,
            "profile_picture": profile_pic,
            "volunteer": volunteer_data
        }
        json_response = json.dumps(response)
        self.response.headers["Content-Type"] = "application/json"
        self.response.out.write(json_response)

app = webapp2.WSGIApplication([
    webapp2.Route('/scrape', handler=LinkledinScrapper, handler_method='post', methods=['POST'])
], debug=True)
